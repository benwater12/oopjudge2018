
/**
 * @author b05505033
 * @since 2018/04/19
 * @version 1.0.2
 <h1>a class to make a pizza order</h1>
 *stores information about a single pizza order.<br>
 *for example 3 pizzas in one order.
 *<p>Change Log<br>
* 1.0.0 : finished all set functions.<br>
* 1.0.1 : finished all functions.<br>
* 1.0.2: found that the commit was incorrect, nothing else was changed..<br>
 */
public class PizzaOrder {
private Pizza Pizza1;//The first pizza
private Pizza Pizza2;//The second pizza
private Pizza Pizza3;//The third pizza
private int number=0;//the number of the pizza in the order, use 0 as intial condition.
/**
 * A func that set number from 1 to 3, returns true if successed in setting
 * return false of the number is invalid.  
 * @param numberPizzas
 * How much the pizzas should be placedin the order.
 * @return
 * a boolean which decide the number is between 1 to 3, or not.
 */

public boolean setNumberPizzas(int numberPizzas)
{
	if(0<numberPizzas&&numberPizzas<4)//rule out above 3 and lower than 1
	{
		number=numberPizzas;
		return true;
	}
return false;
}
/**
 * a function that cacluate the total of the order!<br>
 * Using Pizza methods
 * @return
 * the total cost of the order, using double
 */
public double calcTotal()
{
	if(number==1)
	{
		return Pizza1.calcCost();
	} if(number==2)
	{
		return Pizza1.calcCost()+Pizza2.calcCost();
	}
	if(number==3)
	{
		return Pizza1.calcCost()+Pizza2.calcCost()+Pizza3.calcCost();
	}
	return 0;
}
/**
 /**setting the value of pizza1
 * @param pizza1
 * input the value of pizza1
 */
public void setPizza1(Pizza pizza1) {
	Pizza1 = pizza1;
}
/**setting the value of pizza2
 * @param pizza2
 * input the value of pizza2
 */
public void setPizza2(Pizza pizza2) {
	Pizza2 = pizza2;
}
/**setting the value of pizza3
 * @param pizza3
 * input the value of pizza3
 */
public void setPizza3(Pizza pizza3) {
	Pizza3 = pizza3;
}

/**
 * Testing coding, nothing special thank you!
 * @param afe
 * NONE TO STATE
 */
public static void main(String[] afe)
{
	Pizza pizza1 = new Pizza("large", 1, 0, 1);
	Pizza pizza2 = new Pizza("medium", 2, 2, 5);
	Pizza pizza3 = new Pizza();
	PizzaOrder order = new PizzaOrder();
	System.out.println(order.setNumberPizzas(5));
	order.setNumberPizzas(2);
	order.setPizza1(pizza1);
	order.setPizza2(pizza2);
	System.out.println(order.calcTotal());
	order.setNumberPizzas(3);
	order.setPizza1(pizza1);
	order.setPizza2(pizza2);
	order.setPizza3(pizza3);
	System.out.println(order.calcTotal());
	}
}
