/**
 * @version 1.1
 * @author b05505033 Benson Weng
 * @since 2018/6/14
 *        <h1>a class for ATM OP</h1>
 *        <p>
 * 		Change Log<br>
 *        1.0 : Builded the class<br>
 *        1.1 : Added some commits, and finished!<br>
 *        1.2 : Fixed the issue that I forgot the IO out the e.getmessage()<br>
 */
public class Simple_ATM_Service implements ATM_Service {
	/*
	 * where account is an existing account, and value is an integer. returns a
	 * boolean value true if this checking process is passed successfully.
	 */
	public boolean checkBalance(Account account, int money) throws ATM_Exception {
		if (money > account.getBalance()) {
		//	System.out.println("1"); testing
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
		}
		return true;
	}

	/*
	 * 
	 * where value is an integer returns a boolean value true if this checking
	 * process is passed successfully. true as passed
	 */
	public boolean isValidAmount(int money) throws ATM_Exception {
		if (money % 1000 != 0) {
		//	System.out.println("2"); testing
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
		}
		return true;
	}

	/*
	 * 
	 * where account is an existing account, and value is an integer. returns a
	 * String the same as the name of exception to point out which exception
	 * happened, and no matter what, it gives the current amount.
	 */
	public void withdraw(Account account, int money) {
		boolean valid = false;
		try {
			if (checkBalance(account, money) && isValidAmount(money)) {
				valid = true;
			}
		} catch (ATM_Exception e) {
			//System.out.println("3"); testing
			System.out.println(e.getMessage());
		} finally {
			if (valid) {
				account.setBalance(account.getBalance() - money);
			}
			System.out.println("updated balance : " + account.getBalance());
		}

	}
	/**
	 * testing main plz ignore
	 * 
	 * @param a
	 */
	/*
	 * public static void main(String[] a) { Account David = new Account(4000);
	 * Simple_ATM_Service atm = new Simple_ATM_Service();
	 * System.out.println("---- first withdraw ----"); atm.withdraw(David,1000);
	 * System.out.println("---- second withdraw ----"); atm.withdraw(David,1000);
	 * System.out.println("---- third withdraw ----"); atm.withdraw(David,1001);
	 * System.out.println("---- fourth withdraw ----"); atm.withdraw(David,4000);}
	 */
}
