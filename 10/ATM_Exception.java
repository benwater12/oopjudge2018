/**
 * @version 1.1
 * @author b05505033 Benson Weng
 *@since 2018/6/14
 *<h1>a class for ATM exception</h1>
 *this class is for those who calls exceptions of two conditons
 *<br>amount of money can be divided by 1000, if not, throws an exception named ATM_Exception with type of " AMOUNT_INVALID
 *<br>checking whether balance in user's account is sufficient, if not, throws an exception named ATM_Exception with type of " BALANCE_NOT_ENOUGH"
 *<p>Change Log<br>
* 1.0 : Finished the class!!!<br>
* 1.1 : Added some commits<br>
*/
public class ATM_Exception extends Exception {
enum ExceptionTYPE{BALANCE_NOT_ENOUGH,AMOUNT_INVALID};
ExceptionTYPE excptionCondition;	
/**
 * changes the ExceptionTYPE into a desired one
 * @param amountInvalid
 * ExceptionTYPE which was the error reason.
 */
ATM_Exception(ExceptionTYPE amountInvalid)
	{
		super();
		excptionCondition=amountInvalid;
	}


	@Override
	public String getMessage() {
		return excptionCondition.toString();
	}
}
