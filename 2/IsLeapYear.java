
/**
 * @author b05505033
 *
 */

public class IsLeapYear {

	/**
	 * @param a
	 * this inputs the year required.
	 * @return
	 * a boolean which says this is a leap or not
	 */

	public boolean determine(int a)
	{
		boolean c=false;//setting c as an indicate of leap or not 
	if(a%400==0)//rule divisible by 400 is a leap year
	{ 
		c=true;
	}
	if(a%4==0&a%100!=0)//rule divisible by 4 but not by 100 is a leap year
	{
		c=true;
	}
	return c;	
	}
	public static void main(String[] args) {
		IsLeapYear ily = new IsLeapYear();

		System.out.println(ily.determine(2014));
		System.out.println(ily.determine(2004));
		System.out.println(ily.determine(2100));
	}
	//test coding.
}
