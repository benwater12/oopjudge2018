/**
 * @version 1.0
 * @author b05505033
 *@since 2018/5/26
 *<h1>a class for a circle</h1>
 *stores information about a circle.<br>
 *<p>Change Log<br>
* 1.0.0 : finished all constructuors and functions.<br>
 */
import java.text.DecimalFormat;
import java.lang.Math;
public class Circle extends Shape {



	/**
	 * @param length
	 * used to intialize the diameter of the shape
	 */
	public Circle(double length) {
		super(length);
	}

	@Override
	public void setLength(double length) {
		 this.length=length;

	}


	@Override
	public double getArea() {
		double half=length/2.0;
		double a;
		a=Math.PI*Math.pow(half,2);
		DecimalFormat form=new DecimalFormat("##.00");//this makes sure the format is correct.
		a=Double.parseDouble(form.format(a));//convert the string back to double.
		return a;
	}

	@Override
	public double getPerimeter() {
		double a=length*Math.PI;
		DecimalFormat form=new DecimalFormat("##.00");
		a=Double.parseDouble(form.format(a));
		return a;
	}

}
