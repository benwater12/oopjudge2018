

/**
 * @version 1.0
 * @author b05505033
 *@since 2018/5/26
 *<h1>a class for a circle</h1>
 *stores information about a circle.<br>
 *<p>Change Log<br>
* 1.0.0 : nothing???<br>
 */
public abstract class Shape {
	protected double length;//side length or diameter	

	public Shape(double length){
		this.length=length;
	}
	
	public abstract void setLength(double length);
	
	public abstract double getArea();
	
	public abstract double getPerimeter();
	
	public String getInfo(){
		return "Area = "+getArea()+
			   ", Perimeter = "+getPerimeter();
	}
	public static void main(String[] afe)
	{
		ShapeFactory shapeFactory = new ShapeFactory();
		Shape triangle = shapeFactory.createShape(ShapeFactory.Type.Triangle, 7.5);
		Shape square = shapeFactory.createShape(ShapeFactory.Type.Square, 5);
		Shape circle = shapeFactory.createShape(ShapeFactory.Type.Circle, 5);
		System.out.println(triangle.getInfo());
		System.out.println(square.getInfo());
		System.out.println(circle.getInfo());
		System.out.println(square.getArea() > triangle.getArea());
		System.out.println(square.getPerimeter() > circle.getPerimeter());
		triangle.setLength(10.5);
		square.setLength(3.2);
		circle.setLength(0);
		System.out.println(triangle.getInfo());
		System.out.println(square.getInfo());
		System.out.println(circle.getInfo());
		System.out.println(square.getArea() > triangle.getArea());
		System.out.println(square.getPerimeter() > circle.getPerimeter());
	}
}
