/**
 * @version 1.0
 * @author b05505033
 *@since 2018/5/26
 *<h1>a class for a triangle</h1>
 *stores information about a triangle.<br>
 *<p>Change Log<br>
* 1.0.0 : finished all constructuors and functions.<br>
 */
import java.text.DecimalFormat;
import java.lang.Math;
public class Triangle extends Shape {


	/**
	 * @param length
	 * used to intialize the diameter of the shape
	 */
	public Triangle(double length) {
		super(length);
	}

	@Override
	public void setLength(double length) {
		 this.length=length;

	}

	@Override
	public double getArea() {
		double area=0;
		area=Math.sqrt(3.0)/4.0*Math.pow(length,2);//regular triangle formula.
		DecimalFormat form=new DecimalFormat("##.00");//this makes sure the format is correct.
		area=Double.parseDouble(form.format(area));//convert the string back to double.
		return area;
	}

	@Override
	public double getPerimeter() {
		double a=length*3;
		DecimalFormat form=new DecimalFormat("##.00");
		a=Double.parseDouble(form.format(a));
		return a;
	}

}
