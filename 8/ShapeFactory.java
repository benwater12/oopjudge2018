
/**
 * @version 1.0
 * @author b05505033
 *@since 2018/5/26
 *<h1>a class for a shape maker</h1>
 *this class creates a shape, circle, square, or triangle.<br>
 *<p>Change Log<br>
* 1.0.0 : finished all constructuors and functions.<br>
 */
public class ShapeFactory {
	enum Type{Triangle, Circle, Square};
	/**
	 * @param shapeType
	 * directly insert the type desired,where shapeType can refer to Triangle, Square or Circle
	 * @param length
	 *  represents the side length or diameter of the shape.
	 * @return
	 * a shape which as the inserted side length or diameter.
	 */
	public Shape createShape(ShapeFactory.Type shapeType, double length)
	{
		switch (shapeType)
		{
		case Triangle:
			Shape temp=new Triangle(3);
			temp.setLength(length);
			return temp;
		case Circle:
			Shape temp1=new Circle(5);
			temp1.setLength(length);
			return temp1;
		case Square:
			Shape temp2=new Square(3);
			temp2.setLength(length);
			return temp2;
		default:
			return null;
			
		}
	}
}
