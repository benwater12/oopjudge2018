/*
 * b05505033 
 * Weng Chen-Hsuan
 * ESOE 
 */
public class GreenCrud {
	public int calPopulation(int size, int days)//as suggested size is the orginal size, days is how much days passed
	{
		int fin=size;//the final output, also store the current size
		int store=size;//store the previous number 
		if(days/5<=1)//the first and second tick
		{
		//System.out.println("------------");//testing code
		return fin;
		}
		for(int i=days/5;i>1;--i)//three ticks and above
		{   
			
			fin=fin+store;//save this loop's number
			store=fin-store;//store the previous number
			//System.out.println(store);//testing code
		}
		//System.out.println("------------");//testing code
		return fin;
	}
/public static void main(String[] args) {//testing code
		GreenCrud gc = new GreenCrud();
		System.out.println(gc.calPopulation(6, 15));
		System.out.println(gc.calPopulation(10, 17));
		System.out.println(gc.calPopulation(3, 20));
	}
}
