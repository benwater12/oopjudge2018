

/**
 * @version 1.0.2
 * @author b05505033
 *@since 2018/4/16
 *<h1>a class for a pizza order</h1>
 *stores information about a single pizza.<br>
 *LIKE size of the pizza(either small, medium or large), the number of cheese toppings, the number of pepperoni toppings, and the number of ham toppings.
 *<p>Change Log<br>
* 1.0.0 : finished all constructuors and set functions.<br>
* 1.0.1 : finished all func and get functions.<br>
* 1.0.2: fixed a issue that cause the output of STRING is incorrect.<br>
 */
public class Pizza {
private String size;
private int cheese;
private int pepper;
private int ham;
public Pizza(String size, int cheese, int pepper, int ham) {
	this.size = size;
	this.cheese = cheese;
	this.pepper = pepper;
	this.ham = ham;
}
public Pizza()
{
	size = "small";
	cheese = 1;
	pepper = 1;
	ham = 1;
	}
/**
 * @param Size
 * used to set the size
 */
public void setSize(String Size)
{
	this.size=Size;
}
/**
 * @param NumberOfCheese
 * used to set the cheese number
 */
public void setNumberOfCheese(int NumberOfCheese)
{
this.cheese=NumberOfCheese;
}
/**
 * @param NumberOfPepperoni
 * used to set the pepperoni number
 */
public void setNumberOfPepperoni(int NumberOfPepperoni)
{
	this.pepper=NumberOfPepperoni;
}
/**
 * @param NumberOfHam
 * used to set the ham number 
 */
public void setNumberOfHam(int NumberOfHam)
{
this.ham=NumberOfHam;
}
/**
 * @return
 * as mentioned get the size of pizza
 */
public String getSize()
{ return this.size;
}
/**
 * @return
 * get the cheese number
 */

public int getNumberOfCheese()
{
	return this.cheese;	
} 
/**
 * @return
 * get pepperoni number
 */
public int getNumberOfPepperoni()
{
	return this.pepper;
	}
/**
 * @return
 * get ham number
 */
public int getNumberOfHam()
{
	return this.ham;
}

/**
 * @return
 * calculate the total of the pizza, if size is incorrect, will use "small" as input
 */
public double calcCost()
{
	double a= 0.0;
	if(size.equals("small"))
	{
		a+=10.0+2*(cheese+ham+pepper);
	}
	if(size.equals("medium"))
	{
		a+=12.0+2*(cheese+ham+pepper);
	}
	if(size.equals("large"))
	{
		a+=14.0+2*(cheese+ham+pepper);
	}
	if(size.equals("small")!=true&&size.equals("large")!=true&&size.equals("medium")!=true)
	{
		System.out.println("wrong input, setting default settings");
		size="small";
		a+=10.0+2*(cheese+ham+pepper);
	}
		return a;
}

public String toString()
{
	String name;
	name="size = "+size+", numOfCheese = "+cheese+ ", numOfPepperoni = "+pepper+", numOfHam = "+ham;
	return name;
}
/**
 * @param OtherPizza
 * another pizza which we want to compare
 * @return
 * 
 * a boolean deciding whether the Pizza is identical or not.
 */
public boolean equals(Pizza OtherPizza)
{
	if(this.getNumberOfCheese()!=OtherPizza.getNumberOfCheese())
	{return false;}
	if(this.getNumberOfHam()!=OtherPizza.getNumberOfHam())
	{return false;}
	if(this.getNumberOfPepperoni()!=OtherPizza.getNumberOfPepperoni())
	{return false;}
	if(this.getSize().equals(OtherPizza.getSize())!=true)
	{return false;}
	return true;
}
/**
*
*
*/




/**TESTING CODING
 * @param a
 * nothing 
 * 
 */

//public static void main(String[] a)
//{
//Pizza pizza = new Pizza("large", 3, 1, 5);
//System.out.println(pizza.getSize());
//System.out.println(pizza.getNumberOfCheese());
//System.out.println(pizza.getNumberOfPepperoni());
//System.out.println(pizza.getNumberOfHam());
//pizza = new Pizza();
//pizza.setSize("medium");
//pizza.setNumberOfCheese(2);
//pizza.setNumberOfPepperoni(4);
//pizza.setNumberOfHam(1);
//System.out.println(pizza.getSize());
//System.out.println(pizza.getNumberOfCheese());
//System.out.println(pizza.getNumberOfPepperoni());
//System.out.println(pizza.getNumberOfHam());
//System.out.println(pizza.calcCost());
//System.out.println(pizza.toString());
//System.out.println(pizza.equals(new Pizza("large", 2, 4, 1)));
//System.out.println(pizza.equals(new Pizza()));
//System.out.println(pizza.equals(new Pizza("medium", 2, 4, 1)));
//}
}