/**
 * @version 1.0
 * @author b05505033 Benson Weng
 *@since 2018/6/10
 *<h1>a class for a special exception</h1>
 *<p>Change Log<br>
* 1.0: finished all constructuors.<br>
 */
public class UnknownCmdException extends Exception {
	/**
	 * invalid operator: "[operator] is an unknown operator"
	 * @param a
	 * input unknown String
	 */
	public UnknownCmdException(String a)
	{
		super(a);
	}
	public UnknownCmdException UnknownCmdException(String a)
	{
		return new UnknownCmdException(a+" is an unknown operator");
	}
	/**
	 * invalid value: "[value] is an unknown value"
	 * @param a
	 * @return
	 * UnknownCmdException whic carries the format.
	 * input catch NumberFormatException.
	 */
	public UnknownCmdException UnknownCmdException(NumberFormatException a)
	{ 	String b;
		b=a.getMessage().substring(19);
		b=b.substring(0,1);
		//System.out.println(b);
		return new UnknownCmdException(b+" is an unknown value");
	}
	/**
	 * Both operator and value are unknown
	 * @param a
	 * input catch NumberFormatException.
	 * @param b
	 * input unknown String
	 */
	public UnknownCmdException UnknownCmdException(NumberFormatException b,String c)
	{
		String d;
		d=b.getMessage().substring(19);
		d=d.substring(0,1);
		return new UnknownCmdException(c+" is an unknown operator and "+d+" is an unknown value");
	}
	/**
	 *invalid form: "Please enter 1 operator and 1 value separated by 1 space" 
	 */
	public UnknownCmdException()
	{
		super("Please enter 1 operator and 1 value separated by 1 space");
	}	
	/**
	 *  dividing by zero: "Can not divide by 0"
	 * @param a
	 * input exception a
	 */
	public UnknownCmdException(int a)
	{
		super("Can not divide by 0");
	}

}
