/**
 * @version 1.0
 * @author b05505033 Benson Weng
 *@since 2018/5/26
 *<h1>a class for a calculator</h1>
 serve as a simple calculator. This calculator keeps track of a single number (of type double) that is called result and that starts out as 0.00. Each cycle allows the user to repeatedly add, subtract, multiply, or divide by a second number. The result of one of these operations becomes the new value of result. The calculation ends when the user enters the letter R for result (either in upper- or lowercase).
 *<p>Change Log<br>
* 0.1 : Builded the class<br>
* 1.0 : Finished the class!!!<br>
* 1.1 : Added some commits<br>
 */
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
public class SimpleCalculator {

	
	
/*	public static void main(String[] a)
{
		SimpleCalculator cal = new SimpleCalculator();
		String cmd = null;
		System.out.println(cal.getMsg());
		String cmd_str = "+ 5,- 2,* 5,/ 3,% 2,* D,X D,XD,, ,/ 1000000,/ 00.000,/ 0.000001,+ 1 + 1,- 1.66633,r R,r";
		String[] cmd_arr = cmd_str.split(",");
		for (int i = 0; i < cmd_arr.length; i++) {
		  try {
		    if (cal.endCalc(cmd_arr[i])) 
		      break;		
		    cal.calResult(cmd_arr[i]);
		    System.out.println(cal.getMsg());
		  } catch (UnknownCmdException e) {
		    System.out.println(e.getMessage());
		  }
		}
		System.out.println(cal.getMsg());
}*/

	private double result=0.0;//store the previous result
	private double pass=0.0;//the next number which wanted to be operated
	private String Operator;//the operater
	private int count=0;//keep intrack of the calculator status
	boolean valid = false;//check the operator correct.
	/**
	 * @param string
	 * it should be in the form "'op'(blank)'double'"
	 * @throws UnknownCmdException
	 * the required exception.
	 */
	public void calResult(String string)throws UnknownCmdException  {
		
		try {
	String[] list=string.split(" ");
	if(list.length!=2)
	{
		throw new UnknownCmdException();
	}
	String operator=list[0];
	String number=list[1];
	Operator=operator;
	valid=false;


		//System.out.println(number);

		if(operator.equals("+"))
		{
		pass=0;
		pass=Double.parseDouble(number);
		if(Double.parseDouble(number)==0)
		{throw new UnknownCmdException(0);}
		result+=Double.parseDouble(number);
		
		valid=true;
		}
		if(operator.equals("-"))
		{	pass=0;
		pass=Double.parseDouble(number);valid=true;
		if(Double.parseDouble(number)==0)
		{throw new UnknownCmdException(0);}
		result-=Double.parseDouble(number);
		}
		if(operator.equals("/"))
		{pass=0;
		pass=Double.parseDouble(number);
		valid=true;
		if(Double.parseDouble(number)==0)
		{throw new UnknownCmdException(0);}
		result=result/pass;	
		}
		if(operator.equals("*"))
		{valid=true;
		pass=0;
		pass=Double.parseDouble(number);
		if(Double.parseDouble(number)==0)
		{throw new UnknownCmdException(0);}
		result*=Double.parseDouble(number);	
		}
		if(valid!=true)//case operator isnt +-*/
		{
		pass=Double.parseDouble(number);
		throw new UnknownCmdException().UnknownCmdException(operator);
		}
		}

		catch(NumberFormatException b)//case value is not a number
		{
		if(valid!=true)
		{
			throw new UnknownCmdException().UnknownCmdException(b,Operator);
		}
		else
		{
			throw new UnknownCmdException().UnknownCmdException(b);	
		}
		}
		
	}


	/**
	 * this can halt the calculator.
	 * @param string
	 * using "r and R" as pause code
	 * @return
	 * boolean which tells you that the result of checking
	 */
	public boolean endCalc(String string) {
		if(string.equals("r")|string.equals("R"))
		{count=-1;
			return true;
		}
		return false;
	}

	 /**
	  * translate the current message as a string
	 * @return
	 * string in a format which is required. 
	 */
	public String getMsg() {
		DecimalFormat c=new DecimalFormat("0.00");
		String a=c.format(result);
		String al=c.format(pass);
		if(count==0) {count++;
			return "Calculator is on. Result = "+a;}
		if(count==1) { 
			count=2; 
			return "Result "+Operator+" "+al+" = "+a+". New result = "+a;
		}
		if(count==-1)
		{return "Final result = "+a;}
		return "Result "+Operator+" "+al+" = "+a+". Updated result = "+a;
	 }
}
