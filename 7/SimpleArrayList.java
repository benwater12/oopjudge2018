

/**
 * @author b05505033
 * @since 2018/05/10
 * @version 0.0.1
 <h1>A New Data Structure-a Resizable-array</h1>
 *uses an Integer array as its internal instance variable to save integer, and its size needs to be changed automatically based on user's manipulation.<br>
 *<p>Change Log<br>
* 0.0.1 : finished consturctors, add, set, get, clear, size.<br>
* 0.0.2 : finished remove.<br>
* 0.0.3: huge changes on add, remove.<br>
 */
public class SimpleArrayList {
	private Integer[] list;// the main data storage
	/**
	 * can set initial array size, and set all Integer to zero in the array.
	 * @param size
	 * how long the list is
	 */
	public SimpleArrayList(int size) {
		list=new Integer[size];
		for(int i=0; i<size;i++)
		{
			//System.out.println(i);
			list[i]=0;
		}
		}
	/**
	 * Default constucter
	 */
	public SimpleArrayList() {
		list=null;
		}
	/**
	 * appends the specified element to the end of this array.
	 * @param i
	 * an integer which you want to add at the back.
	 */
	public void add(Integer i) {
		if(list!=null) 
		{
		Integer[] temp=new Integer[list.length+1];
		int count=0;
		for(Integer a:list)
		{
			temp[count]=a;
			count++;
		}
		temp[temp.length-1]=i;
			list=temp;}
		if(list==null)
		{
			list=new Integer [1];
			list[0]=i;
		}
		
	}
	/**
	 *returns the element at the specified position in this array. If the specified position is out of range of the array, returns null. 
	 * @param index
	 * the place where the desired element is
	 * @return
	 * the desired element, or null if out of bounds.
	 */
	public Integer get(int index)
	{
	if(list==null)
	{return null;}
	if(list.length<=index)
	{return null;}
	if(list[index]==null)
	{return null;}
	Integer A=new Integer(list[index]); 
	return A;}
	/**
	 *  replaces the element at the specified position in this array with the specified element, and returns the original element at that specified position. If the specified position is out of range of the array, returns null.
	 * @param index
	 * where you want to place the element(using array)
	 * @param element
	 * the number which you want to place with!!!
	 * @return
	 * null if out of bounds, else will give you the original element at the place
	 */
	public Integer set(int index, Integer element)
	{if(index>=list.length||index<0)
	{
		return null;
	}
	if(list[index]==null)
	{
	list[index]=element;
		return null;}
	Integer temp=new Integer(list[index]);
	list[index]=element;
	return temp;
	}
	 /**
	 *removes all of the elements from this array.
	 */
	public void clear()
	 {
list=null;
	 }
	 /**
	  * If a null element is at the specified position, returns false; otherwise removes it and returns true. Shifts any subsequent elements to the left if removes succesfully.
	 * @param index
	 * the address which you want to remove
	 * @return
	 * tells the removal is sucessful or not
	 */
	public boolean remove(int index)
	 { 
		 if(index>=list.length||index<0)
		{
			return false;
		}
		if(list.length==0||list[index]==null)
		{
			return false;
		}
		
	list[index]=null;
	Integer[] tmp=new Integer[list.length-1];
	int tmpnum=0;
	for(int i=0;i<list.length;i++)
	{
		if(list[i]==null)
		{
			continue;
		}
		tmp[tmpnum]=list[i];
		tmpnum++;
	}
	/*for(int i=0;i<list.length;i++)
	{
	 System.out.println(list[i]);
	}
 for(int i=0;i<tmp.length;i++)
	{
	 System.out.println(tmp[i]);
	}*/
	 list= new Integer[list.length-1];
	for(int i=0;i<list.length;i++)
	{
list[i]=tmp[i];
	}
	 
		 return true;}
	 /**
	  * returns the number of elements in this array.
	 * @return
	 * the length of this array.
	 */
	public int size()
	 { int a=list.length;
		return a;}
	 /**
	  * retains only the elements in this array that are contained in the specified SimpleArrayList. In other words, removes from this array all of its elements that are not contained in the specified SimpleArrayList. Returns true if this array changed as a result. 
	 * @param l
	 * the arraylist which one wanted to compare with 
	 * @return
	 * a boolaen which says the list is changed or not. 
	 */
	public boolean retainAll(SimpleArrayList l)
	 {
		int check=list.length;
		for(int i=0;i<list.length;i++)
		{
		if(contains(l,this.list[i])!=true)
		{
			remove(i);
		}
		}
		if(check!=list.length)
		{return true;}
		return false;
	}
	private static boolean contains(SimpleArrayList a, Integer v) {
	    for (Integer t : a.list) {
	        if (t.equals(v)) {
	            return true;
	        }
	    }
	    return false;
	}
	/**
	 * Testing coding, nothing special thank you!
	 * 
	 * @param args
	 *            NONE TO STATE
	 */
	/*public static void main(String[] args) {
		System.out.println("=== TASK 1 ===");
		SimpleArrayList list = new SimpleArrayList();
		System.out.println(list.get(0));

		System.out.println("=== TASK 2 ===");
		list.add(2);
		list.add(5);
		list.add(8);
		list.add(1);
		list.add(12);
		
		System.out.println(list.get(2));

		System.out.println("=== TASK 3 ===");
		System.out.println(list.get(5));

		System.out.println("=== TASK 4 ===");
		System.out.println(list.set(2, 100));

		System.out.println("=== TASK 5 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 6 ===");
		System.out.println(list.set(5, 100));

		System.out.println("=== TASK 7 ===");
		System.out.println(list.remove(2));

		System.out.println("=== TASK 8 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 9 ===");
		System.out.println(list.remove(2));


		System.out.println("=== TASK 10 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 11 ===");
		System.out.println(list.get(3));

		System.out.println("=== TASK 12 ===");
		list.clear();
		System.out.println(list.get(0));
		

		System.out.println("=== TASK 13 ===");
		SimpleArrayList list2 = new SimpleArrayList(5);
		System.out.println(list2.get(3));

		System.out.println("=== TASK 14 ===");
		System.out.println(list2.get(9));

		System.out.println("=== TASK 15 ===");
		for (int i = 0; i < list2.size(); i++) {
			System.out.println(list2.set(i, i));
		}
		for (int i = 0; i < 5; i++) {
			list.add(i);
		}
		System.out.println(list.retainAll(list2));

		System.out.println("=== TASK 16 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 17 ===");
		System.out.println(list2.remove(0));
		System.out.println(list2.remove(2));
		System.out.println(list.retainAll(list2));

		System.out.println("=== TASK 18 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 19 ===");
		System.out.println(list.set(1, null));
		System.out.println(list.remove(1));

		System.out.println("=== TASK 20 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 21 ===");
		System.out.println(list.set(1, 123));

		System.out.println("=== TASK 22 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 23 ===");
		System.out.println(list.remove(1));

		System.out.println("=== TASK 24 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 25 ===");
		list.add(null);
		System.out.println(list.remove(2));

		System.out.println("=== TASK 26 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

	}
*/
}
