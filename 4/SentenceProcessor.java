
/**
 *@version 1.0.3
*@author b05505033
*@since 2018-04-12
*<h1>TWO works to do in one class</h1>
*<p>removeDuplicatedWords<br>
*return a string, which is sentence after removal of all duplicated words (leaving only the one that occurs first).
*<p>replaceWord<br>
*return a string, which is sentence after replacement of all occurrences of target with replacement.
* <p>Change Log<br>
* 1.0.1 : finished removeDuplicatedWords.<br>
* 1.0.2 : finished replaceWord.<br>
*1.0.3:finished commenting
 */
public class SentenceProcessor {

/**
 * @param target
 * the string which needed to be "de"duplicate
 * @return
 * a string which have no duplicate contained
 */
public String removeDuplicatedWords(String target){
	int length=0;
	int blank=1;//searching words in STRING
	String answer=null;//this stores the final answer
	length=target.length()-1;// getting the STRING lenght
	for(int i=0; i<length;i++)//getting the word count
	{
	if(target.charAt(i)==' ')
	{blank++;}
    }	
		String [] words=new String[blank];
        words=target.split(" ");
       //this line nested for loop is for kicking duplicates
       for (int i = 0; i < words.length; i++)
		{
			for (int j = 0; j < words.length; j++)
			{
				if (words[i].equals(words[j]))
				{
					if (i != j)
					words[j] = "";
				}
			}
		}
       answer=words[0];
       for(int i=1;i<words.length;i++)//make the string
       {
    	   if(words[i]!="")
    	   answer=answer+" "+words[i];
       }
     
		return answer;
	}
	/**
	 * @param target
	 * the word which we wanted to replace
	 * @param replacement
	 * the word which will take over "target's" spot
	 * @param sentence
	 * the entire String we want to adjust
	 * @return
	 * a String which had it's key words exchange as target. 
	 */
	public String replaceWord(String target,String replacement,String sentence)
	{
		int length=0;
		int blank=1;//searching word in STRING
		String answer=null;//this stores the final answer
		length=sentence.length()-1;//gets the length of the sentence
		for(int i=0; i<length;i++)//searching for word count
		{
		if(sentence.charAt(i)==' ')
		{blank++;}
	    }	
			String [] words=new String[blank];
	        words=sentence.split(" ");
	        for(int i=0;i<words.length;i++)
	        {
		        	if(target.equals(words[i]))//Replacing words to target
		        	{
		        		words[i]=replacement;
		        	}
	        }
	        
	        answer=words[0];
	        for(int i=1;i<words.length;i++)//make the string
	        {
	     	   if(words[i]!="")
	     	   answer=answer+" "+words[i];
	        }
	      
	 		return answer;
	}
/**TESTING CODE DONT TAKE IT SERIOUS
 * @param a
 * nothing really
 */
/*public static void main(String[] a) {
SentenceProcessor sp = new SentenceProcessor();

		System.out.println(sp.removeDuplicatedWords("Hello Hello World I love love the World I lovelove the World"));
		System.out.println(sp.removeDuplicatedWords("Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo"));
		System.out.println(sp.removeDuplicatedWords("a a la a la carte A la La carte Carte A a la la"));
		System.out.println(sp.replaceWord("major", "minor", "The major problem is how to sing in A major"));
		System.out.println(sp.replaceWord("on", "off", "Turn on the television I want to keep the television on"));
		System.out.println(sp.replaceWord("love", "hate", "I love the World I lovelove the Love"));
	}*/
	
}
